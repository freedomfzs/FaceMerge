
package com.face.baidu.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.baidu.aip.util.Base64Util;
import com.face.config.BaiduConfig;
import com.face.config.BaiduConfig.Account;
import com.face.constant.RedisConstant;

import cn.hutool.core.util.NumberUtil;
import lombok.extern.slf4j.Slf4j;

/**
* 人脸融合
*/
@Slf4j
@Component
public class FaceMergeUtil {

    @Autowired
    private RedissonClient redissonClient;
    
    @Autowired
    private AuthUtil authUtil;
    
    @Autowired
    private BaiduConfig baiduConfig;
    
    public String faceMerge(String templateImage,String targetImage,Account account) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/face/v1/merge";
        try {
            Map<String, Object> map = new HashMap<>();
            Map<String, Object> image_templateMap = new HashMap<>();
            image_templateMap.put("image", templateImage);
            image_templateMap.put("image_type", "BASE64");
            image_templateMap.put("quality_control", "NONE");
            map.put("image_template", image_templateMap);
            Map<String, Object> image_targetMap = new HashMap<>();
            image_targetMap.put("image", targetImage);
            image_targetMap.put("image_type", "BASE64");
            image_targetMap.put("quality_control", "NONE");
            map.put("image_target", image_targetMap);

            String param = GsonUtils.toJson(map);

            RBucket<Object> accessTokenBucket = redissonClient.getBucket(String.format(RedisConstant.BAIDU_ACCESS_TOKEN, account.getAccountNo()));
            
            String accessToken = (String)accessTokenBucket.get();

            if(StringUtils.isBlank(accessToken)) {
                accessToken = authUtil.getAuth(account.getApiKey(), account.getSecuretKey());
                if(StringUtils.isBlank(accessToken)) {
                    throw new NullPointerException("获取百度accessToken异常");
                }
                accessTokenBucket.set(accessToken, 30*24-1, TimeUnit.HOURS);
            }
            
            String result = HttpUtil.post(url, accessToken, "application/json", param);
            Map resultMap = GsonUtils.fromJson(result, Map.class);
            log.info(result);
            if(((Double)resultMap.get("error_code")).intValue()==0) {
                return (String)((Map)resultMap.get("result")).get("merge_image");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        log.info(Base64Util.encode(IOUtils.toByteArray(FileUtils.openInputStream(ResourceUtils.getFile("classpath:img/s1.jpg")))));
    }
}
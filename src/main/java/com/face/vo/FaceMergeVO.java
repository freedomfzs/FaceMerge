package com.face.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "人臉融合响应類",description = "人臉融合响应類")
public class FaceMergeVO {

    @ApiModelProperty(value="融合图Base64编码",name="imageBase64")
    private String imageBase64;
    
}

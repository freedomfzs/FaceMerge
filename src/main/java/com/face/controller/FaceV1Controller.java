package com.face.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.face.form.FaceMergeForm;
import com.face.service.IFaceMergeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 人脸融合接口
 * @author freedomfzs
 *
 */
@RestController
@RequestMapping("/v1")
@CrossOrigin
@Api(value = "人脸接口",tags = "人脸接口")
public class FaceV1Controller {
    
    @Autowired
    private IFaceMergeService faceMergeService;
    
    @ApiOperation(value = "人脸融合")
    @PostMapping(value = "/merge")
    public ResponseEntity<String> merge(@Valid @RequestBody FaceMergeForm form) {
        return faceMergeService.mergeV1(form);
    }

}
package com.face.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FaceMergeTemplateEnum {

    T0001("0001","t0001.png","圆梦美丽中国"),//
    T0002("0002","t0002.png","圆梦美丽中国"), //
    T0003("0003","t0003.jpg","我为党庆生") //
    ;
    private String code;
    private String fileName;
    private String desc;
}

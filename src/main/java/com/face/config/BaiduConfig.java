package com.face.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Configuration
@PropertySource(value = {"classpath:/application.yml"}, encoding = "utf-8")
@ConfigurationProperties(prefix = "baidu")
@Data
public class BaiduConfig {

    private Map<String, Account> accounts;

    @Data
    public static class Account{
        
        private String accountNo;
        
        private String apiKey;
        
        private String securetKey;
    }
}
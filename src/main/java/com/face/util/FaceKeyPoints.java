package com.face.util;

import org.bytedeco.opencv.global.opencv_face;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.*;
import org.bytedeco.opencv.opencv_face.Facemark;
import org.bytedeco.opencv.opencv_face.FacemarkLBF;
import org.springframework.util.ResourceUtils;

public class FaceKeyPoints {
	// 加载人脸检测器（Face Detector）
	// [1]Haar Face Detector
	// CascadeClassifier faceDetector("haarcascade_frontalface_alt2.xml");
	// [2]LBP Face Detector 
	// 创建Facemark类的对象
	static Facemark facemark = FacemarkLBF.create();
	static{
		try{
			facemark.loadModel(ResourceUtils.getFile("classpath:face/data/lbpcascades/lbpcascade_frontalface.xml").getAbsolutePath());
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static Point2fVectorVector piontsSixEight(Mat frame){
		Mat gray = new Mat();   
		// 将视频帧转换至灰度图, 因为Face Detector的输入是灰度图
		opencv_imgproc.cvtColor(frame, gray, opencv_imgproc.COLOR_BGR2GRAY);
		// 存储人脸矩形框的容器
		RectVector faces = CheckFaceAndEye.findFaces(gray);
		// 人脸关键点的容器
		Point2fVectorVector landmarks = new Point2fVectorVector();
		boolean success = faces != null;
		if(success){ 
			// 运行人脸关键点检测器（landmark detector）
			success = facemark.fit(frame, faces, landmarks);
		}
		
		if(success){
			return landmarks;
		}else{
			return null;
		}
	}
}
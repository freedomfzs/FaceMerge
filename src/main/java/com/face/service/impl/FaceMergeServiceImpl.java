package com.face.service.impl;

import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.baidu.aip.util.Base64Util;
import com.face.baidu.util.FaceMergeUtil;
import com.face.config.BaiduConfig;
import com.face.config.BaiduConfig.Account;
import com.face.form.FaceMergeForm;
import com.face.service.IFaceMergeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FaceMergeServiceImpl implements IFaceMergeService {

    @Autowired
    private BaiduConfig baiduConfig;
    
    @Autowired
    private FaceMergeUtil faceMergeUtil;
    
    @Override
    public ResponseEntity<String> mergeV1(FaceMergeForm form) {
        String filePath = "classpath:template/"+form.getImageTemplateType().getFileName();
        try {
            String imageTemplateBase64 = Base64Util.encode(IOUtils.toByteArray(FileUtils.openInputStream(ResourceUtils.getFile(filePath))));
            
            for (Map.Entry<String, Account> entry : baiduConfig.getAccounts().entrySet()) {
                String mergeBase64 = faceMergeUtil.faceMerge(imageTemplateBase64, form.getImageBase64(),entry.getValue());
                if(!StringUtils.isBlank(mergeBase64)) {
                    return ResponseEntity.ok(mergeBase64);
                }
            }
            
        } catch (Exception e) {
            log.error("模板图片{}加载异常",filePath,e);
        }
        
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        
    }

}

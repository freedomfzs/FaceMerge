package com.face.service;

import org.springframework.http.ResponseEntity;

import com.face.form.FaceMergeForm;

public interface IFaceMergeService {
    ResponseEntity<String> mergeV1(FaceMergeForm form);
}

package com.face.vfile;

import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.IplImage;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Size;

import java.io.File;

public class Vido {
    static boolean exit = false;

    public static void main(String[] args) throws Exception {
        System.out.println("start...");
        String rtmpPath = "E:/face/vido/file.flv";
        String rtspPath = "E:/face/vido/file2.flv"; //oflaDemo
        //String rtspPath = "rtmp://58.200.131.2:1935/livetv/gxtvvvvv";
        int audioRecord = 1; // 0 = 不录制，1=录制
        boolean saveVideo = false;
        push(rtmpPath, rtspPath, audioRecord, saveVideo);

        System.out.println("end...");
    }

    public static void push(String rtmpPath, String rtspPath, int audioRecord, boolean saveVideo) throws Exception {
        // 使用rtsp的时候需要使用 FFmpegFrameGrabber，不能再用 FrameGrabber
        int width = 640, height = 480;
        FFmpegFrameGrabber grabberImg = new FFmpegFrameGrabber(rtmpPath);
       // FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault(rtmpPath);
        grabberImg.start();
        FFmpegFrameGrabber grabberA = new FFmpegFrameGrabber(rtmpPath);
        System.out.println("grabber start");
        grabberA.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(rtspPath, grabberImg.getImageWidth(), grabberImg.getImageHeight(), grabberA.getAudioChannels());
        recorder.setOption("rtsp_transport", "tcp");
        recorder.setInterleaved(true);
        // recorder.setVideoOption("crf","28");
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // 28
        recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
        recorder.setFormat("flv"); // rtmp的类型
        recorder.setFrameRate(25);
        recorder.setPixelFormat(0); // yuv420p
        System.out.println("recorder start");
        recorder.start();
        //
        OpenCVFrameConverter.ToIplImage conveter = new OpenCVFrameConverter.ToIplImage();
        System.out.println("all start!!");
        int count = 0;
        while (!exit) {
            count++;
            //视频
            Frame frameI = grabberImg.grabImage();
            //视频+音频
            //Frame frame = grabber.grab();
            //音频 grabber.grabSamples()
            Frame frameA = grabberA.grabSamples();
           // Frame frame = grabber.grabFrame(true, true, true, false);
            if (frameI == null || frameA == null) {
                continue;
            }
            String frame1 = "E:/face/1FACE.jpg";//单张图片的位置
            //Mat img = opencv_imgcodecs.imread(frame1);
            //Mat reImg = new Mat();
            //opencv_imgproc.resize(img,reImg,new Size(width,height));
            //opencv_imgcodecs.imwrite("E:/face/vido"+ File.separator  + "1111temp.jpg", reImg);

            //IplImage image =  opencv_imgcodecs.cvLoadImage(frame1); // 非常吃内存！！
            //Frame frame2 = conveter.convert(image);
           // recorder.recordImage(frameI.imageWidth,frameI.imageHeight,frameI.imageDepth,frameI.imageChannels,frameI.imageStride,avcodec.AV_CODEC_ID_MOTIONPIXELS,frameI.image);
           // recorder.record(frame2);  //录制
            // 释放内存
            //opencv_core.cvReleaseImage(image);
            recorder.record(frameI);
            recorder.setTimestamp(grabberImg.getTimestamp());  //告诉录制器这个audioSamples的音频时长
            recorder.recordSamples(frameA.samples);
            //recorder.record(frame);  //录入音频
            System.out.println(count);
        }

        grabberImg.stop();
        grabberImg.release();
        grabberA.stop();
        grabberA.release();
        recorder.stop();
        recorder.release();
    }
}

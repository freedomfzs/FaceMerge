package com.face.vfile;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;

import java.io.File;
import java.io.FileOutputStream;

public class Aoudio {
    public static void main(String[] args) {
        String saveAudioFile = "E:/face/vido" + File.separator + 1 + ".mp4";

        FFmpegFrameGrabber ff = null;
        FFmpegFrameRecorder recorder = null;
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(saveAudioFile);
            ff = new FFmpegFrameGrabber("E:/face/vido/file.flv");
            // rtsp 默认udp 丢包 改为tcp
            ff.setOption("rtsp_transport", "tcp");
            ff.start();
            System.out.println(ff.getAudioChannels());
            recorder = new FFmpegFrameRecorder(fos, ff.getAudioChannels());
            recorder.setAudioCodec(ff.getAudioCodec());
           // recorder.setAudioOption("ar", "16000");
            //recorder.setFormat("s16le");
            recorder.start();
            Frame frame = null;

            int frame_number = ff.getLengthInFrames();  // 帧总数
            for (int i = 1; i < frame_number; i++) {
                frame = ff.grabSamples();
                if (null != frame) {
                    recorder.record(frame);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}